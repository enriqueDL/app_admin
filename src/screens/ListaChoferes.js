import { Form } from "native-base";
import React, {useState, useEffect} from "react";
import {View, Text, FlatList} from "react-native";
import TasklistChofer from '../screens/TasklistChofer';
import  api from '../utils/Api';

const ListaChoferes = () => {
    const [datosChofer, setDatosChofer] = useState([])


    const listarChoferes = async () => {
        var repuesta = await api.get('/api/v1/driver');
        setDatosChofer(repuesta.data.data)
        console.log('repuestaCh--->', repuesta);
        console.log('setDatosChofer--->', setDatosChofer);
    }

    useEffect(() => {
        listarChoferes()
    }, [])


    return(

        <View>
            <TasklistChofer datosChofer={datosChofer}/>
        </View>
    )
}


export default ListaChoferes;