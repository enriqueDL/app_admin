import React, {Component, useState, useEffect } from 'react';
import {View, Text, StyleSheet, Dimensions, ToastAndroid, 
  Alert, Modal, ActivityIndicator, Image, TouchableOpacity,
   TouchableHighlight, StatusBar, FlatList, SafeAreaView} from 'react-native';
import * as Location from 'expo-location';
import Map from './Map';
import GetDistancia from '../components/getDistancia';
import * as SecureStore from 'expo-secure-store';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { mapStyle } from '../styles/mapStyle';
import { FAB } from 'react-native-paper';
import { FloatingAction } from "react-native-floating-action";
import modalDistancia from '../components/ModalDist';
import  globalStyles from '../styles/global';
import { Container, Content, List, ListItem, Card, CardItem,  } from 'native-base';
import  api from '../utils/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';


const Mapa = ({route, navigation}) => {

  const { rol } = route.params;
  const { token } = route.params;
  const { datoProp } = route.params;
  const { modal } = route.params;
 

  //const {navigation} = props;
  console.log('mensaje desde Mapa navigation', navigation)
  console.log('mensaje desde Mapa datoProp', datoProp)
  console.log('mensaje desde Mapa modal', modal)

  const value = AsyncStorage.getItem('key_dat'); 
  console.log('dato de AsyncStorage ', value);
  //console.log('mensaje del rol desde la parte de arriba ', rol)
  //const { token } = route.params;
  SecureStore.setItemAsync('rol', rol);
  SecureStore.setItemAsync('key_token', token);

  const [position, setPosition] = useState(null);
  const [markersTransport, setMarkersTransport] = useState([]);
  const [markersParadas, setMarkersParadas] = useState([]);
  const [markersEst, setMarkersEst] = useState([]);
  const [markersDist, setMarkersDist] = useState([]);
  const [markersChof, setMarkersChofer] = useState([]);
  const [ isloadin, guardarIsloadin] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalGeol, setModalGeol] = useState(modal);

 


  
  

  const getPosition = async () => {
    try {
      const { coords } = await Location.getCurrentPositionAsync({});
      console.log('posicion', coords);
      /*setMarkers([
        {
          latitude: coords.latitude,
          longitude: coords.longitude,
          message: "Transporte",
        },
      ]);*/
     await GuardarLatitlongit(coords)
     var dt = await MostarLatiLogit()
     console.log("datos transport", dt)
     setMarkersTransport(dt)

     /*for (var dat in dt) {
       console.log('dato del for de arriba dat', dat)
       console.log('dato del for de arriba dt', dt)
     }*/
     /*var distancia = GetDistancia(dt[0].key.latitude, dt[0].key.longitude, dt[2].key.latitude, dt[2].key.longitude)
     console.log('Dato del metodo----- MostarLatiLogit---->', dt)
     console.log('Dato de la posicion 0----- MostarLatiLogit---->' + 'latitude ' + dt[0].key.latitude + ' longitude--> ', + dt[0].key.longitude) 
     console.log('Dato de la posicion 2----- MostarLatiLogit---->',+ 'longuitude ', + dt[2].key.latitude + ' longitude--> ', + dt[2].key.longitude)
     console.log('Distancia entro los dos puntos---->', distancia)*/
      //ejemplo()
      //var datossss = await MostarLatiLogit();
     //console.log("dentro de la funcion getPosition a ver si me muestra MostarLatiLogit datossss", datossss)
    }catch (error) {
      console.log("getPosition -> error", error);
    }
  };

  const entryPoint = async () => {
   
    try {
      const { status } = await Location.requestPermissionsAsync();
      if (status === "granted") {
        getPosition();
        //setModalGeol(true);
  
      }else{
        ToastAndroid.show('Se denegó el permiso para acceder a la ubicación', ToastAndroid.SHORT);
        console.log("markersEst", markersEst)
        setModalGeol(false);
      }
    }catch (error) {
      console.log("getPermissionAndPosition -> error", error);
  
    }
  };

  useEffect(() => {
   
    isAuthToken();
    MostrarParadas();
    
  },[]);
  useEffect(() => {
      setTimeout(() => {
        guardarIsloadin(false);
      },
      10)
  }, []);

  useEffect(() => {
   
        //isAuthToken();
        entryPoint();
        //setModalGeol(false)
        //ObtenerLatiLogitChofer();
    
  }, [markersEst]);

  


//Enviando el token

async function isAuthToken(){
  setModalGeol(true);
        let token = await SecureStore.getItemAsync('key_token');
           console.log('espera..');

         
          let repuest =  api.defaults.headers['x-access-token'] = token;
        
}


async function GuardarLatitlongit(coords){

  let latitude = coords.latitude
  let longitude = coords.longitude
  let rol = await SecureStore.getItemAsync('rol');

  //console.log('rol del studiante ', rol)

  console.log('coords', coords);
  const datos = {
    'latitude': latitude,
    'longitude': longitude,
    'rol': rol}

    try {
    

      var repuesta = await api.post('/api/v1/coordinates/create', datos); 

      console.log('Repuesta de la vista del mapa', repuesta);
        
    }catch (error) {
      console.log(error);
      guardarIsloadin(false);
        Alert.alert('', 'En este momento el servicio no esta disponible');
    }

}



  async function MostarLatiLogit(){
    
    try {
      
      var repuesta = await api.get('/api/v1/users/actives'); 

      console.log('activos-->', repuesta)
      var dat = repuesta.data.dataUser 

      console.log('repuesta de los usuario activos dat repuesta.data.dataUser --->', dat)
      console.log('dato modalGeol --->', modalGeol)

     //console.log('Repuesta de la vista del mapa obteniendo los usuarios activos-->', repuesta.data);
      //setMarkers(datedrive)
        //var datedrive = []

        var ChoferLatLong = []
        var EstudianteLatLong = []
        //setMarkersChofer(EstudianteLatLong)
    
      for (var dato in dat ) {

         if (dat[dato].hasOwnProperty('key')) {
          console.log("for prueba---->", dat[dato])

          
          if (dat[dato].key != null) {
            console.log("datos del for--->", dat[dato].key)
          
            var repuest = await api.get(`/api/v1/coordinates/${dat[dato].key}`)
            //datedrive.push({key:repuest.data})

            console.log('Repuesta obteniendo las cordenadas 1111----->', repuest)
            if (repuest.data.rol=='driver') {
              ChoferLatLong.push({key:repuest.data})
              console.log('datos de las cordenadas del chofer--->', repuest.data)
            }else if(repuest.data.rol=='student') {
                EstudianteLatLong.push({key:repuest.data})
                console.log('datos de las coordenadas del estudiante--->', repuest.data)
            }
            

          }
         }

        
          
      }

      setModalGeol(false)

      setMarkersEst(EstudianteLatLong)
     

    
     
      console.log('arreglo ChoferLatLong-->', ChoferLatLong)
    

      return ChoferLatLong;

    
    }catch (error) {
      console.log(error);
      guardarIsloadin(false);
        Alert.alert('', 'En este momento el servicio no esta disponible');
    }

  }


  async function MostrarParadas(){

    var paradas = []
    var repuesta = await api.get('/api/v1/stops'); 
    
    var datosRepuest = repuesta.data.data
    console.log("Paradas", repuesta.data.data)
    for(var dato in datosRepuest){
      console.log("forPardas", repuesta.data.data[dato])
      paradas.push({key:repuesta.data.data[dato]})
      //console.log("forPar", dato)
    }


    setMarkersParadas(paradas)
   
  }

  console.log("datos del marker intententando 1233444---> ", markersTransport)
  console.log('markersEst 1 ------>', markersEst)
  //console.log('arrayParadas------>', paradas)
  console.log('markersParadas------>', markersParadas)


  function ChangeModalVisible(bool){
    setModalVisible(bool);
  }

  // const ChangeModalVisible = (bool) => {
  //   setModalVisible(bool);
  // }
 
  function closeModal(bool){
    setModalVisible(bool);
  }
//  const closeModal = (bool, data) => {
//     setModalVisible(bool);
//   }

   

  return(

    

    <View style={styles.container}>
      <MapView
        customMapStyle={mapStyle}
        provider={PROVIDER_GOOGLE}
        style={styles.mapStyle}
        initialRegion={{
          latitude: 9.91152,
          longitude: -67.35381,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,

        }}
        mapType="mutedStandard"
        minZoomLevel={14}
       >
        {markersTransport.map((marker, index, key) => (
          <Marker
            key={index}
            coordinate={{
              'latitude': marker.key.latitude,
              'longitude': marker.key.longitude,
            }}
            image={require('../assets/buss.png')}
            title={marker.key.latitude.toString()}
            description={marker.key.longitude.toString()}

          />
        ))}
        {markersEst.map((marker, index, key) => (
          <Marker
            key={index}
            coordinate={{
              'latitude': marker.key.latitude,
              'longitude': marker.key.longitude,
            }}
            image={require('../assets/student.png')}
            title={key.id, marker.key.rol}
          />
        ))}

        {markersParadas.map((marker, index, key) => (
          <Marker
            key={index}
            coordinate={{
              'latitude': marker.key.latitude,
              'longitude': marker.key.longitude,
            }}
            
            //image={require('../assets/paradas1.jpg')} style={{width:'-100%', height:'-100%' }}
            title={key.id, marker.key.name}
          />
        ))}

      </MapView>

   
    
     
      {modalGeol==true 
                      ?  
                      <Modal
                        animationType="fade"
                        transparent={true}
                        visible={modalGeol}
                        onRequestClose={false}>
                        <View style={styles.centeredViewGeo}>
                              <View style={styles.modalViewGeo}>

                                  <ActivityIndicator  size="large" color="#0000ff" />

                                  <View style={styles.modalTextGeo}> 
                                    <Text >Cargando Geolocalización...</Text>
                                  </View>
                                 
                              </View>
                          
                        </View>
                      </Modal>
                      : 
                        <Modal
                          animationType="fade"
                          transparent={true}
                          visible={false}
                          onRequestClose={false}>
                          <View style={styles.centeredViewGeo}>
                              <View style={styles.modalViewGeo}>
                                  <ActivityIndicator size="large" color="#0000ff" />

                                  <Text style={styles.modalTextGeo}>Cargando...</Text>
                              </View>
                          
                          </View>
                        </Modal>
      }
     
      

     <StatusBar barStyle={'default'} />
     
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  fab: {
    position: 'absolute',
    margin: 20,
    right: 0,
    bottom: 0,
  },
  enteredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    flex:1,
    margin: 100,
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 100,
    paddingHorizontal: 10,
  
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  modalViewGeo: {
    width: '80%',
    height: '22%',
    backgroundColor: 'white',
    borderRadius: 20,
    paddingHorizontal: '10%',
    paddingVertical: '10%',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    width: 200,
    marginTop: -90,
    textAlign: 'center',
    borderRadius: 10,
    backgroundColor: '#ADADAD'
   
  },

  modalTextGeo: {
    textAlign: 'center',
    marginTop: 10
  },
  containerM: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  centeredView: {
    flex: 1,
    alignItems: 'center',


  },

  centeredViewGeo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30
  },
  containerSafeA: {
    marginTop: 20,
    paddingHorizontal: -4,
   
   

  },
  DatoDialogDistancCard : {
    flexDirection: 'column'
    
   
  },

  DatoDialogTiempo : {
    paddingRight: 95
  },

  buttonsView: {
    justifyContent: 'flex-start',
   
 
  },
  touchableOpacity: {
    width: 60,
    height: 20,
    marginTop: 90,
    borderRadius: 10,
    backgroundColor: 'red',
    marginVertical: 10,
    marginHorizontal: '28%',
    padding: 7,
  },
  TextDis: {
    width: 70,
    marginTop: 30,
    justifyContent: 'flex-start',
    
  },
  linea: {
    marginTop: 10,
    borderRadius: 10,
    width: 190,
    height: 4,
    backgroundColor: '#ADADAD'
  },
   linea2: {
    marginTop: -50,
    borderRadius: 10,
    width: 190,
    height: 4,
    backgroundColor: '#ADADAD'
  },
  modalSal: {
    justifyContent: 'center',
  }
  
});



export default Mapa;