import React, {useState, useEffect} from 'react';
import {Text, View, Button, TextInput, TouchableHighlight, ImageBackground, StyleSheet, Image} from 'react-native';
import  globalStyles from '../styles/global';
import  api from '../utils/Api';





function Agregarchofer() {
	
	const [ name, guardarName] = useState('');
	const [ lastName, guardarlastName] = useState('');
	const [ email, guardarEmail] = useState('');
	const [ unidad, guardarUnidad] = useState('');
	const [ password, guardarPassword] = useState('');

	const CrearChofer = async () => {
		const datos = { name, lastName, email, password };
		var repuesta = await api.post('/api/v1/driver/create', datos);
		
		ToastAndroid.show(repuesta, ToastAndroid.SHORT);

		console.log('CrearChofer--->', repuesta);
		
	}

	return (
		<View style={globalStyles.container}>
			 <ImageBackground source={require('../assets/pantalla2.png')} 
                style={globalStyles.image} > 
				 <Image style={styles.LogoUser} source={require('../assets/profile.png')} />
				<Text style={styles.TextoRegistro}>Registrar chofer!</Text>

				<TextInput style={globalStyles.input} 
					placeholder="name"
					placeholderTextColor="#969696"
					onChangeText={ texto => guardarName(texto) }
					value={name}
							
				/>

				<TextInput style={globalStyles.input} 
					placeholder="lastName"
					placeholderTextColor="#969696"
					onChangeText={ texto => guardarlastName(texto) }
					value={lastName}
							
				/>

				<TextInput style={globalStyles.input} 
					placeholder="Email"
					placeholderTextColor="#969696"
					onChangeText={ texto => guardarEmail(texto) }
					value={email}
							
				/>

				<TextInput style={globalStyles.input} 
					placeholder="Unidad"
					placeholderTextColor="#969696"
					onChangeText={ texto => guardarUnidad(texto) }
					value={unidad}
							
				/>

				<TextInput style={globalStyles.input} 
					placeholder="Password"
					onChangeText={ texto => guardarPassword(texto) }
					value={password}
					placeholderTextColor="#969696"
					secureTextEntry={true}
				/>

				<TouchableHighlight onPress={ () => CrearChofer() } style={globalStyles.btnSubmit}>
					<Text style={globalStyles.textoSubmit}> Crear </Text>
				</TouchableHighlight>
			</ImageBackground>

		</View>
	);
} 

export default Agregarchofer;


const styles = StyleSheet.create({
	LogoUser: {
		width: 80,
		height: 80,
		  marginBottom: 36,
		  marginVertical: 10,
		  marginHorizontal: '40%',
	
	  },
	  TextoRegistro: {
		fontSize: 18,
		textAlign: "center",
		marginTop: -20,
		color: "white"
	
	  },
	  

})